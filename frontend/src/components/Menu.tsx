import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";
import { Link } from "react-router-dom";
import { Routes } from "../routes";

type MenuProps = {
  items: Routes;
};

export const Menu = ({ items }: MenuProps) => {
  const itemKeys = Object.keys(items);

  return (
    <Nav className="mr-auto" navbar>
      {itemKeys.map((i) => (
        <NavItem key={i}>
          <NavLink tag={Link} to={items[i].path}>
            {items[i].text}
          </NavLink>
        </NavItem>
      ))}
    </Nav>
  );
};
