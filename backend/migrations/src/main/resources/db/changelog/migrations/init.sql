
CREATE TABLE medication_request
(
    ID  SERIAL PRIMARY KEY,
    name text NOT NULL,
    mobile VARCHAR(20) NOT NULL,
    city text NOT NULL,
    requirement text NOT NULL,
    created_at timestamp with time zone
);


CREATE TABLE medication_offer
(
    ID  SERIAL PRIMARY KEY,
    name text NOT NULL,
    mobile VARCHAR(20) NOT NULL,
    city text NOT NULL,
    offer text NOT NULL,
    created_at timestamp with time zone
);
