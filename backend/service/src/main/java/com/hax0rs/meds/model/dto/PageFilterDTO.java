package com.hax0rs.meds.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@RequiredArgsConstructor
@Validated
public class PageFilterDTO {

    private SortBy sortBy = SortBy.id;
    private Sort.Direction sortDirection = Sort.Direction.DESC;
    @ApiModelProperty(value = "search query over text field")
    private String query;

    @ApiModelProperty(value = "search query over city")
    private String city;

    @ApiModelProperty(value = "page", required = true,
            allowableValues = "range[1, +infinity]")
    @Min(1)
    int page = 1;

    @ApiModelProperty(value = "page size", required = true,
            allowableValues = "range[1, +infinity]")
    @Min(1)
    int pageSize = 10;
}
