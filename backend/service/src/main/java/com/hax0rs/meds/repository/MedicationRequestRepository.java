package com.hax0rs.meds.repository;

import com.hax0rs.meds.model.entity.MedicationOffer;
import com.hax0rs.meds.model.entity.MedicationRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicationRequestRepository extends JpaRepository<MedicationRequest, Long> {

    Page<MedicationRequest> findAll(Pageable pageable);

    Page<MedicationRequest> findByRequirementContainingIgnoreCase(String requirement, Pageable pageable);

    Page<MedicationRequest> findByCityContainingIgnoreCase(String city, Pageable pageable);

    Page<MedicationRequest> findByCityContainingIgnoreCaseAndRequirementContainingIgnoreCase(String city, String requirement, Pageable pageable);

    void deleteByMobileContainingIgnoreCase(String mobile);
}
