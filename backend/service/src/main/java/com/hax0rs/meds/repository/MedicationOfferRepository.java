package com.hax0rs.meds.repository;

import com.hax0rs.meds.model.entity.MedicationOffer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationOfferRepository extends JpaRepository<MedicationOffer, String> {


    Page<MedicationOffer> findAll(Pageable pageable);

    Page<MedicationOffer> findByOfferContainingIgnoreCase(String offer, Pageable pageable);


    Page<MedicationOffer> findByCityContainingIgnoreCase(String city, Pageable pageable);

    Page<MedicationOffer> findByCityContainingIgnoreCaseAndOfferContainingIgnoreCase(String city, String offer, Pageable pageable);

    void deleteByMobileContainingIgnoreCase(String mobile);
}
