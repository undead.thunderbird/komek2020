package com.hax0rs.meds.controller;

import com.hax0rs.meds.model.dto.MedicationOfferDTO;
import com.hax0rs.meds.model.dto.MedicationRequestDTO;
import com.hax0rs.meds.model.dto.PageFilterDTO;
import com.hax0rs.meds.model.entity.MedicationOffer;
import com.hax0rs.meds.model.entity.MedicationRequest;
import com.hax0rs.meds.service.MedsService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class MedsController {

    private final MedsService medsService;
    private final String secret = "<SECRET_KEY>";

    @ApiOperation(
            value = "submit request",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PostMapping("/request/submit")
    @CrossOrigin
    public ResponseEntity submitRequest(@Valid @RequestBody MedicationRequestDTO medicationRequestDTO) {
        medsService.submitRequest(medicationRequestDTO);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(
            value = "submit offer",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PostMapping("/offer/submit")
    @CrossOrigin
    public ResponseEntity submitOffer(@Valid @RequestBody MedicationOfferDTO medicationOfferDTO) {
        medsService.submitOffer(medicationOfferDTO);
        return ResponseEntity.ok().build();
    }


    @ApiOperation(
            value = "get offers",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @GetMapping("/offer/list")
    @CrossOrigin
    public Page<MedicationOffer> getOffers(@Validated PageFilterDTO pageFilter) {

        return medsService.getOffers(pageFilter);
    }


    @ApiOperation(
            value = "delete offers",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @DeleteMapping("/offer")
    @CrossOrigin
    public String deleteOffer(@RequestParam String mobile, @RequestParam String secret) {
        if (this.secret.equals(secret)) {
            medsService.deleteOffer(mobile);
            return "delete attempt was made";
        }
        return "error";
    }

    @ApiOperation(
            value = "delete request",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @DeleteMapping("/request")//make delete later
    @CrossOrigin
    public String deleteRequest(@RequestParam String mobile, @RequestParam String secret) {
        if (this.secret.equals(secret)) {
            medsService.deleteRequest(mobile);
            return "delete attempt was made";
        }
        return "error";
    }

    @ApiOperation(
            value = "get requests",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @GetMapping("/request/list")
    @CrossOrigin
    public Page<MedicationRequest> getRequests(@Validated PageFilterDTO pageFilter) {
        return medsService.getRequests(pageFilter);
    }


}
